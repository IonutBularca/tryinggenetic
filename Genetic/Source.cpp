#include <iostream>

#include<cstdlib>
#include<ctime>

#include <fstream>

#include "Function.h"
#include "RandomGen.h"
#include "GeneticAlgo.h"

void main() {
//	std::cout << std::fixed<< Function::f("1000000111101");

	//correct the function for getting rid of negative values

	const int correction{ 161 };

	Function::correct(correction);

	srand(time(0));

	GeneticAlgo try1;

	try1.init();
	
	std::cout << "best initial : " <<try1.best()<<" "<< " dec :"<<Function::toDecimal(try1.best())<<" " << -(Function::f(try1.best()) - correction) << std::endl;

	std::ofstream f("out.txt");

	f << "#start" << std::endl;
	f << "#generation number " << 0 << std::endl;
	try1.printPopulation(f);
	
//	try1.printPopulation();

	for (int i = 1; i <= 100; i++) {

		f << "#generation number " << i << std::endl;

		try1.select();

		try1.crossover();

		try1.printPopulation(f);
		
		std::cout << "best : " << try1.best() << " " << " dec :" << Function::toDecimal(try1.best()) << " " << -(Function::f(try1.best())-correction) << std::endl;


		
		
	}

//	try1.printPopulation();
		f << "#end" << std::endl;


		f << "Minimum/maximum chromosome: " << try1.getBestSoFar() << std::endl;
		f << "Minimum/maximum value: " << -(Function::f(try1.getBestSoFar())-correction) << std::endl;


	std::cout << "BEST: " << try1.getBestSoFar() << " " << " dec :" << Function::toDecimal(try1.getBestSoFar()) << " " << -(Function::f(try1.getBestSoFar())-correction) << std::endl;
}