#include <vector>
#include <string>
#pragma once
class GeneticAlgo
{
public:
	 void init();
	 void printPopulation(std::ostream& os) const;
	 void select();
	 void crossover();
	 std::string best() ;

public:

	std::string getBestSoFar() {return bestSoFar;};

public:
	static std::pair<std::string, std::string> cross(const std::string& s1, const std::string& s2);
private:
	 const int NR{150};
	 static const int k{ 13 };
	 const int gen{ 100 };
	 const float pc{ 0.25 };
private:
	 std::vector<std::string> chromoLot;
	 std::string bestSoFar{};
};

