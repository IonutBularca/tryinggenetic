#include <iostream>
#include <vector>
#include <string>

#pragma once
	class Function
	{
	public:

		static float f(const float x) ;
		static double f(const std::string& chromo) ;
		static bool check(const std::string& chromo) ;

		static std::string toDomanin(const std::string& chomo);

		static float toDecimal(const std::string& chromo);

		static std::string toChromo(const float nr);

		static bool better(const std::string& chromo1, const std::string& chromo2);

		static void correct(const int correction);
	
	
	private:


		static const int nArgs{ 1 };
		static const  std::vector<std::pair<int, int>> domain;
		static const int k{ 13};
		static  int correctN ;

	};


