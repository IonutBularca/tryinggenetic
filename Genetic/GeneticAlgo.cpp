#include "GeneticAlgo.h"
#include "RandomGen.h"
#include "Function.h"
#include <random>


void GeneticAlgo::init()
{
	while (chromoLot.size() < NR) {
		std::string newChromo = RandomGen::generateChromo(k);
		if (Function::check(newChromo)) {
			chromoLot.push_back(newChromo);
		}
	}
}

void GeneticAlgo::printPopulation(std::ostream& os) const
{
	const int correction = 161;
	for (auto&& iter : chromoLot) {
	//	std::cout << iter << std::endl;
		os << "Chromosome " << iter <<std::endl<< "value " << -(Function::f(iter) - correction) << std::endl;
	}
//	std::cout << "SIZE: " << chromoLot.size() << std::endl;
}

void GeneticAlgo::select()
{
	float sum = 0;
	std::vector<float> selectionProbability;
	std::vector<float> cumulativeProbability;
	//calculate sum
	for (auto&& iter : chromoLot) {
		sum += Function::f(iter);
		
	}
	//calculate selectionProbability and cumulative probability
	for (auto&& iter : chromoLot) {
		selectionProbability.push_back(Function::f(iter) / sum);
	}
	

	cumulativeProbability.push_back(selectionProbability[0]);
	for (int i = 1; i < NR; i++) {
		cumulativeProbability.push_back(cumulativeProbability[i - 1] + selectionProbability[i]);
	}

	std::default_random_engine generator;
	std::uniform_real_distribution<float> distribution(0, 1);

	std::vector<float> randomDistrib;
	for (int i = 0; i < NR; i++) {
		randomDistrib.push_back(distribution(generator));
	}

	//create the new population

	std::vector<std::string> newPopulation;

	for (auto&& iter : randomDistrib) {
		if (0 < iter && iter <= cumulativeProbability[0]) {
			newPopulation.push_back(chromoLot[0]);
			continue;
		}
		for (int i = 0; i < NR - 1 ; i++) {
			if (iter<=cumulativeProbability[i + 1] && iter > cumulativeProbability[i]) {
				//found the appropriate interval
				newPopulation.push_back(chromoLot[i + 1]);
				break;
			}
		}
	}
	chromoLot.clear();
	chromoLot = newPopulation;

}

void GeneticAlgo::crossover()
{
	std::default_random_engine generator;
	std::uniform_real_distribution<float> distribution(0, 1);

	std::vector<float> randomDistrib;
	for (int i = 0; i < NR; i++) {
		randomDistrib.push_back(distribution(generator));
	}

	std::vector<std::string> forCrossover;
	std::vector<int> toDel;

	for (int i = 0; i < NR; i++) {
		if (randomDistrib[i] < pc) {
			forCrossover.push_back(chromoLot[i]);
			toDel.push_back(i);
		}
	}
	if (forCrossover.size() % 2 != 0) {
		toDel.erase(toDel.begin() + toDel.size() -1 );
		forCrossover.erase(forCrossover.begin() + forCrossover.size() - 1 );
	}

	for (int i = toDel.size()-1; i >= 0; i--) {
		chromoLot.erase(chromoLot.begin() + toDel[i]);
	}


	//--------

	std::vector<std::string> newPopulation;
	for (int i = 0; i < forCrossover.size(); i += 2) {
		std::pair<std::string, std::string> crs = cross(forCrossover[i], forCrossover[i+1]);
		newPopulation.push_back(crs.first);
		newPopulation.push_back(crs.second);
	}
	for (auto&& iter : chromoLot) {
		newPopulation.push_back(iter);
	}

	chromoLot.clear();
	chromoLot = newPopulation;
}

std::string GeneticAlgo::best() 
{
	std::string best = chromoLot[0];
	for (auto&& iter : chromoLot) {
		if (Function::better(iter, best)) {
			best = iter;
		}
	}

	if (bestSoFar == "") {
		bestSoFar = best;
	}
	else {
		if (Function::better(best, bestSoFar)) {
			bestSoFar = best;
		}
	}

	return best;
}

std::pair<std::string, std::string> GeneticAlgo::cross(const std::string& s1, const std::string& s2)
{
	int t = rand() % k;
	int a = t;
	std::string first, second;
	first = s1.substr(0, t) + s2.substr(t , k);
	second = s2.substr(0,t) + s1.substr(t , k);
	if (!Function::check(first) || !Function::check(second))
	{
		return std::make_pair(s1, s2);
	}
	return std::make_pair(first, second);
}
