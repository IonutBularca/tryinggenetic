#include "Function.h"

#include <iomanip>

const std::vector<std::pair<int, int>> Function::domain = { std::make_pair(-10,10) };
int Function::correctN{ 0 };

//here is the function
float Function::f(const float x)
{
	return (-x * x - 6 * x - 1) + correctN;
}

double Function::f(const std::string& chromo)
{
	float dec = toDecimal(chromo);
	return f(dec);
}

bool Function::check(const std::string& chromo)
{
	std::vector<float> splitted;
	std::string ch = chromo;
	while(ch.size()>0) {
		splitted.push_back(toDecimal(ch.substr(0,k)));
		ch.erase(ch.begin(), ch.begin()+k);
	}
	
	for (int i = 0; i < splitted.size(); i++) {
		if (splitted[i]<domain[i].first || splitted[i] > domain[i].second) {
			return false;
		}
	}

	return true;
}

//implement the formula that brings the chromosome into domain
std::string Function::toDomanin(const std::string& chromo)
{

	std::string ch = chromo;
	std::string result{};
	while (true) {
		std::string part = ch.substr(0, k - 1);
		//transform (part);
		
	}

	return 0;
}

float Function::toDecimal(const std::string& chromo)
{
	int integerPart, fractionalPart;
	integerPart = std::stoi(chromo.substr(1, k/2), nullptr, 2);
	fractionalPart = std::stoi(chromo.substr(k/2, k-1), nullptr, 2);
	int x{ fractionalPart };
	int length{ 1 };
	while (x /= 10)
		length++;

	float result{(float)integerPart + ((float)fractionalPart / (float)pow(10,length))};
	if (chromo[0] == '1') {
		result *= -1;
	}
	return result;
}

std::string Function::toChromo(const float nr)
{
	return std::string();
}


bool Function::better(const std::string& chromo1, const std::string& chromo2)
{
	return (f(chromo1) > f(chromo2));
}

void Function::correct(const int correction)
{
	correctN += correction;
}


