#include "RandomGen.h"
#include <sstream>

std::string RandomGen::generateChromo(const int length)
{
	std::stringstream result;
	while (result.str().size() < length) {
		result << rand() % 2;
	}
	return result.str();
}
